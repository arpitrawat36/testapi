const express = require("express");
const router = express.Router();

const data = [];

router.post("/getmo", (req, res) => {
    data.push(req.body);
    res.sendStatus(200);
});

router.get("/getmo", (req, res) => {
    if (req.query['hub.verify_token'] == 'memytoken')
        res.status(200).send(req.query['hub.challenge']);
    else
        res.status(401).send({});
});

router.get("/getmo1", (req, res) => {
    if (req.query) {
        data.push({ "query": req.query });
        res.status(200).send({});
    } else
        res.status(401).send({});
});

router.get("/getmoo", (req, res) => {
    res.status(200).send({ data });
});

module.exports = router;
