const env = { PORT: 9090 };

const mainRouter = require("./routes/main");

const express = require("express");
const app = express();
const server = require("http").createServer(app);

app.use(express.urlencoded({ extended: true, limit: "1kb" }));
app.use(express.json({ limit: "1kb" }));

app.use('/api', mainRouter);

const server_port = process.env.YOUR_PORT || process.env.PORT || env["PORT"];
const server_host = process.env.YOUR_HOST || '0.0.0.0';

server.listen(server_port, server_host, function() {
    console.log('Listening on port ', server_port);
});
